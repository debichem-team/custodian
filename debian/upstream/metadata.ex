# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/custodian/issues
# Bug-Submit: https://github.com/<user>/custodian/issues/new
# Changelog: https://github.com/<user>/custodian/blob/master/CHANGES
# Documentation: https://github.com/<user>/custodian/wiki
# Repository-Browse: https://github.com/<user>/custodian
# Repository: https://github.com/<user>/custodian.git
